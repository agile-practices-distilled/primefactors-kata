using System.Linq;
using NUnit.Framework;

namespace PrimeFactors
{
    public class PrimeFactorsShould
    {
        [TestCase(2,new []{2})]
        [TestCase(3,new []{3})]
        [TestCase(4,new []{2,2})]
        [TestCase(5,new []{5})]
        [TestCase(6,new []{2,3})]
        [TestCase(9,new []{3,3})]
        [TestCase(12,new []{2,2,3})]
        [TestCase(15,new []{3,5})]
        [TestCase(60,new []{2,2,3,5})]
        public void get_its_prime_factors(int input, int[] expectedResult)
        {
            Assert.AreEqual(expectedResult.ToList(), PrimeFactors.Get(input));
        }
    }
}