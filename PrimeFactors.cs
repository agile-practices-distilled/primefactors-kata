﻿using System.Collections.Generic;
using System.Linq;

namespace PrimeFactors
{
    public class PrimeFactors
    {
        public static List<int> Get(int input)
        {
            int primeNumber = 2;
            while (input > 1)
            {
                if (input % primeNumber == 0)
                {
                    return new List<int>{primeNumber}.Concat(Get(input / primeNumber)).ToList();
                }

                primeNumber++;
            }

            return new List<int>();
        }
    }
}