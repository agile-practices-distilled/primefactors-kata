# Primer factors

Factorize a positive integer number into its prime factors using TDD and the TPP table.48 Examples:

### number
2 3 4 6 9 12 15
### prime factors
[2] [3] [2,2] [2,3] [3,3] [2,2,3] [3,5]